//
//  GithubRepo.swift
//  JSON Demo
//
//  Created by Kenneth L. Graham on 12/13/15.
//  Copyright © 2015 Ken Graham. All rights reserved.
//

import UIKit

class GithubRepo: NSObject {
    let name: String
    let owner: String
    let language: String
    let repoDescription: String
    let url: String
    let accountType: String
    
    init(name: String, owner: String, language: String, description: String,
        url: String, accountType: String) {
            self.name = name
            self.owner = owner
            self.language = language
            self.repoDescription = description
            self.url = url
            self.accountType = accountType
            
            super.init()
    }
    
    func isUserAccount() -> Bool{
        return self.accountType == "User"
    }
    
    override var description: String {
        return "name: \(name), owner: \(owner), language: \(language), type: \(accountType)"
    }
}


