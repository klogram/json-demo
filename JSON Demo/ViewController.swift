//
//  ViewController.swift
//  JSON Demo
//
//  Created by Kenneth L. Graham on 12/13/15.
//  Copyright © 2015 Ken Graham. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var dataArray = [GithubRepo]()
    
    // If search query has spaces, each block of whitespace must be replaced by a '+'
    let query = "probability"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        Alamofire.request(.GET, "https://api.github.com/search/repositories?q=\(query)&page=1")
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let value = response.result.value {
//                        var repoArray = [GithubRepo]()
                        let json = JSON(value)

                        for (_, repoData) in json["items"] {
                            let name = repoData["name"].stringValue
                            let url = repoData["url"].stringValue
                            let lang = repoData["language"].stringValue
                            let desc = repoData["description"].stringValue
                            let owner = repoData["owner"]
                            let accountType = owner["type"].stringValue
                            
                            let repo = GithubRepo(name: name, owner: owner["login"].stringValue, language: lang, description: desc, url: url, accountType: accountType)
                            self.dataArray.append(repo)
                            print("\(repo)")
                        }
                        
                        self.tableView?.reloadData()
                    }
                case .Failure(let error):
                    print(error)
                }
                
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // TableView DataSource and Delegate functions
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("\(dataArray.count)")
        return 5
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
//        let repo = dataArray[indexPath.row]

        cell.textLabel?.text = NSDate().description// repo.name
//        cell.detailTextLabel?.text = repo.owner
//        print("\(repo)")
        
        return cell
    }
    
    
    
}

